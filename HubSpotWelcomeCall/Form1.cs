﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HubSpotWelcomeCall.Program;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;

namespace HubSpotWelcomeCall
{
    public partial class Form1 : System.Windows.Forms.Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void btnWelcomeCall_Click(object sender, EventArgs e)
        {
            var connetionString = ConfigurationManager.ConnectionStrings["Application_DatabaseConnection"].ConnectionString;
            SqlConnection sqlConnection = new SqlConnection(connetionString);

            WelcomeCallClass connection = new WelcomeCallClass();

            string sList;

            string date;

            //Day of week to pull the list from
            if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
            { date = DateTime.Now.AddDays(-3).ToShortDateString(); }
            else { date = DateTime.Now.AddDays(-2).ToShortDateString(); }


            if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
            {


                DataTable sqlDt = new DataTable();

                string sqlSelect = "select TOP(10) CC.FriendlyId as CLTID,CN.FirstName as FNAME, CN.LastName as LNAME, ce.Email as EMAIL, cd.BirthDate as CLTDOB " +
                                    " from client.contract as CC" +
                                    " join client.contractapplicant as cap on cap.contractid = CC.id" +
                                    " join Client.Name as CN on CN.ClientId = cap.clientid" +
                                    " join Client.Email as CE ON CE.clientid = cap.clientid" +
                                    " join Client.Detail  as CD on CD.Id = cap.clientid" +
                                    " left outer join client.CancelDetails ccd on ccd.ContractId = CC.id" +
                                    " where ce.Email like '%@%.%' and CC.CreatedDate between  '11/1/2022'  and  '" + date + "' and CCd.id is null " +
                                    " and CC.welcomecallid <>3 or cc.welcomecallid is null";

                SqlCommand sqlCommand = new SqlCommand(sqlSelect, sqlConnection);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCommand);
                sqlDa.Fill(sqlDt);


                int currentTable = 0;

                /*for (var rowIndex = 0; rowIndex < sqlDt.Rows[0].Table.Rows.Count; rowIndex++)
                {
                    if (rowIndex % 150 == 0)
                    {
                        if (rowIndex > 0)
                            currentTable += 1;
                        
                        sqlDt.Rows.Add();
                        
                        //5 is the current number of columns being used. If we change the query, we'll have to change this number
                        if(sqlDt.Rows[1].Table.Rows[currentTable].Table.Columns.Count < 5)
                        {
                            foreach (DataColumn col in sqlDt.Rows[0].Table.Columns)
                                sqlDt.Rows[1].Table.Rows[currentTable].Table.Columns.Add(col.ColumnName, col.DataType);
                        }
                        
                    }
                    sqlDt.Rows[1].Table.Rows[currentTable].Table.ImportRow(sqlDt.Rows[0].Table.Rows[rowIndex]);
                }*/
                if (sqlDt.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt.Rows[0].Table.Rows)
                    {
                       
                        string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.WELCOMECALLISTA = '" + date + "' " +
                            "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                        sqlConnection = new SqlConnection(connetionString);
                        sqlConnection.Open();
                        SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                        myCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                    }
                }

                //Below I think was only necessary when we were dividing up the 
                //result set into 150 row tables
                /*try
                {
                    if (sqlDt.Rows.Count > 1)
                    {
                        foreach (DataRow row in sqlDt.Rows[1].Table.Rows)
                        {


                            string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET HSCLIENT.WELCOMECALLISTB = '" + date + "' " +
                                                "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                            sqlConnection = new SqlConnection(connetionString);
                            sqlConnection.Open();
                            SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                            myCommand.ExecuteNonQuery();
                            sqlConnection.Close();
                        }
                    }
                }

                catch (System.IndexOutOfRangeException)
                {
                    MessageBox.Show("Table 1 doesn't exist! *Welcome Call*", "Welcome Call Fields Updated");
                }
                
                try
                {
                    if (sqlDt.Rows.Count > 2)
                    {
                        foreach (DataRow row in sqlDt.Rows[2].Table.Rows)
                        {


                            string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.WELCOMECALLISTB = '" + date + "' " +
                                                "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                            sqlConnection = new SqlConnection(connetionString);
                            sqlConnection.Open();
                            SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                            myCommand.ExecuteNonQuery();
                            sqlConnection.Close();
                        }
                    }
                }
                catch (System.IndexOutOfRangeException)
                {
                    MessageBox.Show("Table 2 doesn't exist! *Welcome Call*", "Welcome Call Fields Updated");
                }*/

                sList = "Welcome A";
                connection.List = sList;


                DataTable sqlDt1 = new DataTable();

                //STFLG=0 
                //VERCLDTE Is Null AND 
                //CANTYP Is Null AND 
                string sqlSelect1 = "SELECT HS.CLTID, HS.HSVID, HS.WELCOMECALLISTA " +
                                    "FROM hubspot.HSCLIENT HS " +
                                    "WHERE HS.WELCOMECALLISTA = '" + date + "'";

                sqlConnection = new SqlConnection(connetionString);
                SqlCommand sqlCommand1 = new SqlCommand(sqlSelect1, sqlConnection);

                SqlDataAdapter sqlDa1 = new SqlDataAdapter(sqlCommand1);
                sqlDa1.Fill(sqlDt1);

                if (sqlDt1.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt1.Rows[0].Table.Rows)//gvs.DS1.Tables[0].Rows
                    {
                        string sHSVID = row["HSVID"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + sHSVID + "/profile?property=welcome_call_list_a");

                        connection.updateURL = url;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.updateRequest();
                        //this line is pointless because a success is a 204 - 'No Content', thus no real response.
                        //System.IO.File.AppendAllText(@"C:\HubSpot\UpdateTest3.csv", strResponse3);
                    }
                }

                /*sList = "Welcome B";
                connection.List = sList;

                DataTable sqlDt2 = new DataTable();
                string sqlSelect2 = "SELECT HS.CLTID, HS.HSVID, HS.WELCOMECALLISTB " +
                                    "FROM hubspot.HSCLIENT HS " +
                                    "WHERE HS.WELCOMECALLISTB = '" + date + "'";

                sqlConnection = new SqlConnection(connetionString);
                SqlCommand sqlCommand2 = new SqlCommand(sqlSelect2, sqlConnection);

                SqlDataAdapter sqlDa2 = new SqlDataAdapter(sqlCommand2);
                sqlDa2.Fill(sqlDt2);

                if (sqlDt2.Rows.Count > 1)
                {
                    foreach (DataRow row in sqlDt2.Rows[0].Table.Rows)// gvs.DS1.Tables[0].Rows)
                    {
                        string sHSVID = row["HSVID"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + sHSVID + "/profile?hapikey=27223930-be9c-4e1e-b60f-0c59d42c3b10&property=welcome_call_list_b");

                        connection.updateURL = url;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.updateRequest();

                        //System.IO.File.AppendAllText(@"C:\HubSpot\UpdateTest3.csv", strResponse3);
                    }
                }*/


                //string vSQL = "UPDATE CLIENTS SET STFLG = " + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() +
                //    "WHERE CLTID<34299999 AND EMAIL LIKE '%@%.%' AND STFLG=0 AND ACQDTE Between '1/1/2015' And '" + date + "' AND CANCEL Is Null AND VERCLDTE Is Null AND CANTYP Is Null AND CANREAS Is Null";
                //gvs.conn1.Open();
                //SqlCommand cmd = new SqlCommand(vSQL, gvs.conn1);
                //cmd.ExecuteNonQuery();
                //gvs.conn1.Close();
            }

            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                int currentTable = 0;
                DataTable sqlDt = new DataTable();

                //STFLG=0 
                //VERCLDTE Is Null AND 
                //CANTYP Is Null AND 
                string sqlSelect = "select TOP(10) CC.FriendlyId as CLTID,CN.FirstName as FNAME, CN.LastName as LNAME, ce.Email as EMAIL, cd.BirthDate as CLTDOB " +
                                    " from client.contract as CC" +
                                    " join client.contractapplicant as cap on cap.contractid = CC.id" +
                                    " join Client.Name as CN on CN.ClientId = cap.clientid" +
                                    " join Client.Email as CE ON CE.clientid = cap.clientid" +
                                    " join Client.Detail  as CD on CD.Id = cap.clientid" +
                                    " left outer join client.CancelDetails ccd on ccd.ContractId = CC.id" +
                                    " where ce.Email like '%@%.%' and CC.CreatedDate between  '11/1/2022'  and  '" + date + "' and CCd.id is null " +
                                    " and CC.welcomecallid <>3 or cc.welcomecallid is null";

                SqlCommand sqlCommand = new SqlCommand(sqlSelect, sqlConnection);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCommand);
                sqlDa.Fill(sqlDt);



                if (sqlDt.Rows.Count > 0)
                {
                    /*for (var rowIndex = 0; rowIndex < sqlDt.Rows[0].Table.Rows.Count; rowIndex++)
                    {
                        if (rowIndex % 150 == 0)
                        {
                            if (rowIndex > 0)
                                currentTable += 1;
                            //gvs.DS2.Tables.Add();
                            sqlDt.Rows.Add();
                            //foreach (DataColumn col in gvs.DS1.Tables[0].Columns)
                            //    gvs.DS2.Tables[currentTable].Columns.Add(col.ColumnName, col.DataType);
                            foreach (DataColumn col in sqlDt.Rows[0].Table.Columns)
                                sqlDt.Rows[1].Table.Rows[currentTable].Table.Columns.Add(col.ColumnName, col.DataType);
                        }

                        //gvs.DS2.Tables[currentTable].ImportRow(gvs.DS1.Tables[0].Rows[rowIndex]);
                        sqlDt.Rows[1].Table.Rows[currentTable].Table.ImportRow(sqlDt.Rows[0].Table.Rows[rowIndex]);
                    }*/

                    foreach (DataRow row in sqlDt.Rows[0].Table.Rows)
                    {

                        string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.WELCOMECALLISTA = '" + date + "' " +
                                            "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                        sqlConnection = new SqlConnection(connetionString);
                        sqlConnection.Open();
                        SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                        myCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                    }
                }


                //Below I think was only necessary when we were dividing up the 
                //result set into 150 row tables
                /*try
                {
                    if (sqlDt.Rows.Count > 1)
                    {
                        foreach (DataRow row in sqlDt.Rows[1].Table.Rows)
                        {
                            
                            string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET HSCLIENT.WELCOMECALLISTB = '" + date + "' " +
                                                "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                            sqlConnection = new SqlConnection(connetionString);
                            sqlConnection.Open();
                            SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                            myCommand.ExecuteNonQuery();
                            sqlConnection.Close();
                        }
                    }
                }
                catch (System.IndexOutOfRangeException)
                {
                    //MessageBox.Show("Table 1 doesn't exist! *Welcome Call*", "Welcome Call Fields Updated");
                }

                try
                {
                    if (sqlDt.Rows.Count > 2)
                    {
                        foreach (DataRow row in sqlDt.Rows[2].Table.Rows)
                        {
                            string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.WELCOMECALLISTB = '" + date + "' " +
                                                "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                            sqlConnection = new SqlConnection(connetionString);
                            sqlConnection.Open();
                            SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                            myCommand.ExecuteNonQuery();
                            sqlConnection.Close();
                        }
                    }
                }
                catch (System.IndexOutOfRangeException)
                {
                    MessageBox.Show("Table 2 doesn't exist! *Welcome Call*", "Welcome Call Fields Updated");
                }*/

                sList = "Welcome A";
                connection.List = sList;

                DataTable sqlDt1 = new DataTable();
                string sqlSelect1 = "SELECT HS.CLTID, HS.HSVID, HS.WELCOMECALLISTA " +
                                    "FROM hubspot.HSCLIENT HS " +
                                    "WHERE HS.WELCOMECALLISTA = '" + date + "'";

                sqlConnection = new SqlConnection(connetionString);
                SqlCommand sqlCommand1 = new SqlCommand(sqlSelect1, sqlConnection);

                SqlDataAdapter sqlDa1 = new SqlDataAdapter(sqlCommand1);
                sqlDa1.Fill(sqlDt1);
                if (sqlDt1.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt1.Rows[0].Table.Rows)
                    {
                        string sHSVID = row["HSVID"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + sHSVID + "/profile?property=welcome_call_list_a");

                        connection.updateURL = url;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.updateRequest();

                        //System.IO.File.AppendAllText(@"C:\HubSpot\UpdateTest3.csv", strResponse3);
                    }
                }


                /*sList = "Welcome B";
                connection.List = sList;

                DataTable sqlDt2 = new DataTable();
                string sqlSelect2 = "SELECT HS.CLTID, HS.HSVID, HS.WELCOMECALLISTB " +
                                    "FROM hubspot.HSCLIENT HS" +
                                    "WHERE HS.WELCOMECALLISTB = '" + date + "'";

                sqlConnection = new SqlConnection(connetionString);
                SqlCommand sqlCommand2 = new SqlCommand(sqlSelect2, sqlConnection);

                SqlDataAdapter sqlDa2 = new SqlDataAdapter(sqlCommand2);
                sqlDa2.Fill(sqlDt2);

                if (sqlDt2.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt2.Rows[0].Table.Rows)
                    {
                        string sHSVID = row["HSVID"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + sHSVID + "/profile?hapikey=27223930-be9c-4e1e-b60f-0c59d42c3b10&property=welcome_call_list_b");

                        connection.updateURL = url;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.updateRequest();

                        //System.IO.File.AppendAllText(@"C:\HubSpot\UpdateTest3.csv", strResponse3);
                    }
                }*/
            }

            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
            {
                int currentTable = 0;
                DataTable sqlDt = new DataTable();

                string sqlSelect = "select TOP(10) CC.FriendlyId as CLTID,CN.FirstName as FNAME, CN.LastName as LNAME, ce.Email as EMAIL, cd.BirthDate as CLTDOB " +
                                    " from client.contract as CC" +
                                    " join client.contractapplicant as cap on cap.contractid = CC.id" +
                                    " join Client.Name as CN on CN.ClientId = cap.clientid" +
                                    " join Client.Email as CE ON CE.clientid = cap.clientid" +
                                    " join Client.Detail  as CD on CD.Id = cap.clientid" +
                                    " left outer join client.CancelDetails ccd on ccd.ContractId = CC.id" +
                                    " where ce.Email like '%@%.%' and CC.CreatedDate between  '11/1/2022'  and  '" + date + "' and CCd.id is null" +
                                    " and CC.welcomecallid <>3 or cc.welcomecallid is null";

                SqlCommand sqlCommand = new SqlCommand(sqlSelect, sqlConnection);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCommand);
                sqlDa.Fill(sqlDt);


                /*for (var rowIndex = 0; rowIndex < sqlDt.Rows[0].Table.Rows.Count; rowIndex++)
                {
                    if (rowIndex % 150 == 0 && sqlDt.Rows.Count > 0)
                    {
                        if (rowIndex > 0)
                            currentTable += 1;
                        //gvs.DS2.Tables.Add();
                        sqlDt.Rows.Add();
                        foreach (DataColumn col in sqlDt.Rows[0].Table.Columns)
                            sqlDt.Rows[1].Table.Rows[currentTable].Table.Columns.Add(col.ColumnName, col.DataType);
                    }

                    if (sqlDt.Rows.Count > 1)
                        sqlDt.Rows[1].Table.Rows[currentTable].Table.ImportRow(sqlDt.Rows[0].Table.Rows[rowIndex]);
                }*/

                if (sqlDt.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt.Rows[0].Table.Rows)
                    {
                        string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.WELCOMECALLISTA = '" + date + "' " +
                                            "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                        sqlConnection = new SqlConnection(connetionString);
                        sqlConnection.Open();
                        SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                        myCommand.ExecuteNonQuery();
                        sqlConnection.Close();

                    }
                }


                //Below I think was only necessary when we were dividing up the 
                //result set into 150 row tables
                /*try
                {
                    if (sqlDt.Rows.Count > 1)
                    {
                        foreach (DataRow row in sqlDt.Rows[1].Table.Rows)
                        {
                            string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.WELCOMECALLISTB = '" + date + "' " +
                                "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                            sqlConnection = new SqlConnection(connetionString);
                            sqlConnection.Open();
                            SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                            myCommand.ExecuteNonQuery();
                            sqlConnection.Close();
                        }
                    }

                }
                catch (System.IndexOutOfRangeException)
                {
                    MessageBox.Show("Table 1 doesn't exist! *Welcome Call*", "Welcome Call Fields Updated");
                }

                try
                {
                    if (sqlDt.Rows.Count > 2)
                    {
                        foreach (DataRow row in sqlDt.Rows[2].Table.Rows)
                        {
                            string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.WELCOMECALLISTB = '" + date + "' " +
                                "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                            sqlConnection = new SqlConnection(connetionString);
                            sqlConnection.Open();
                            SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                            myCommand.ExecuteNonQuery();
                            sqlConnection.Close();

                        }
                    }

                }
                catch (System.IndexOutOfRangeException)
                {
                    MessageBox.Show("Table 2 doesn't exist! *Welcome Call*", "Welcome Call Fields Updated");
                }
                */
                sList = "Welcome A";
                connection.List = sList;

                DataTable sqlDt1 = new DataTable();
                string sqlSelect1 = "SELECT HS.CLTID, HS.HSVID, HS.WELCOMECALLISTA " +
                                    "FROM hubspot.HSCLIENT HS " +
                                    "WHERE HS.WELCOMECALLISTA = '" + date + "'";

                sqlConnection = new SqlConnection(connetionString);
                SqlCommand sqlCommand1 = new SqlCommand(sqlSelect1, sqlConnection);

                SqlDataAdapter sqlDa1 = new SqlDataAdapter(sqlCommand1);
                sqlDa1.Fill(sqlDt1);

                if (sqlDt1.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt1.Rows[0].Table.Rows)
                    {
                        string sHSVID = row["HSVID"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + sHSVID + "/profile?property=welcome_call_list_a");

                        connection.updateURL = url;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.updateRequest();

                        //System.IO.File.AppendAllText(@"C:\HubSpot\UpdateTest3.csv", strResponse3);
                    }
                }


                /*sList = "Welcome B";
                connection.List = sList;

                DataTable sqlDt2 = new DataTable();
                string sqlSelect2 = "SELECT HS.CLTID, HS.HSVID, HS.WELCOMECALLISTA " +
                                    "FROM hubspot.HSCLIENT HS " +
                                    "WHERE HS.WELCOMECALLISTA = '" + date + "'";

                sqlConnection = new SqlConnection(connetionString);
                SqlCommand sqlCommand2 = new SqlCommand(sqlSelect1, sqlConnection);

                SqlDataAdapter sqlDa2 = new SqlDataAdapter(sqlCommand2);
                sqlDa1.Fill(sqlDt2);

                if (sqlDt2.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt2.Rows[0].Table.Rows)
                    {
                        string sHSVID = row["HSVID"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + sHSVID + "/profile?hapikey=27223930-be9c-4e1e-b60f-0c59d42c3b10&property=welcome_call_list_b");

                        connection.updateURL = url;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.updateRequest();

                        //System.IO.File.AppendAllText(@"C:\HubSpot\UpdateTest3.csv", strResponse3);
                    }
                }*/


                //string vSQL = "UPDATE CLIENTS SET STFLG = " + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() +
                //    "WHERE CLTID<34299999 AND EMAIL LIKE '%@%.%' AND STFLG=0 AND ACQDTE Between '1/1/2015' And '" + date + "' AND CANCEL Is Null AND VERCLDTE Is Null AND CANTYP Is Null AND CANREAS Is Null";
                //gvs.conn1.Open();
                //SqlCommand cmd = new SqlCommand(vSQL, gvs.conn1);
                //cmd.ExecuteNonQuery();
                //gvs.conn1.Close();
            }
            MessageBox.Show("Completed! *Welcome Call*", "Welcome Call Fields Updated");
            Environment.Exit(0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback =
             delegate (object s, X509Certificate certificate,
                      X509Chain chain, SslPolicyErrors sslPolicyErrors)
             { return true; };
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            btnWelcomeCall.PerformClick();
        }
    }
}
 