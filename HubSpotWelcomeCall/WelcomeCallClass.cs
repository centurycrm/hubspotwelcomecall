﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotWelcomeCall
{
    public enum httpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    class WelcomeCallClass
    {
        public string List { get; set; }
        public string updateURL { get; set; }

        public string updateRequest()
        {
            //Grabs todays date and converts it to milliseconds because thats how HS reads in dates
            var date = (DateTime.Now.Month.ToString("d2") + "/" + DateTime.Now.Day.ToString("d2") + "/" + DateTime.Now.Year.ToString("d4"));
            var datecon = Convert.ToDateTime(date);
            var today = (datecon - new DateTime(1970, 1, 1)).TotalMilliseconds;

            string strResponseValue = string.Empty;

            //Depending on what List is equal to then one of the cases will be preformed
            switch (List)
            {
                case "Welcome A":
                    strResponseValue = "{\"properties\":[{\"property\": \"welcome_call_list_a\",\"value\": \"" + today + "\"}]}";
                    break;
                case "Welcome B":
                    strResponseValue = "{\"properties\":[{\"property\": \"welcome_call_list_b\",\"value\": \"" + today + "\"}]}";
                    break;
                default:
                    strResponseValue = "null";
                    break;
            }

            string AccessToken = "pat-na1-b85c99b7-2c6e-4eb7-a0c1-4953f98528bb";
            HttpWebRequest request3 = (HttpWebRequest)WebRequest.Create(updateURL);
            request3.Headers.Add("Authorization", "Bearer " + AccessToken);
            request3.ContentType = "application/json; charset=utf-8";
            request3.Method = "POST";
            request3.Accept = "application/json; charset=utf-8";

            using (var streamWriter = new StreamWriter(request3.GetRequestStream()))
            {
                streamWriter.Write(strResponseValue);
            }
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request3.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        try
                        {
                            throw new ApplicationException("error code:" + response.StatusCode.ToString());
                        }
                        catch (ApplicationException et)
                        {
                            //throw new ApplicationException("error code:");
                        }
                    }
                    //Process the response stream... (could be JSON, XML or HTML ect...)

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            using (StreamReader reader = new StreamReader(responseStream))
                            {
                                strResponseValue = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = e.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        strResponseValue = sr.ReadToEnd();
                    }
                }
            }

            return strResponseValue;
        }
    }
}
