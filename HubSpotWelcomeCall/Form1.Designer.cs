﻿namespace HubSpotWelcomeCall
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnWelcomeCall = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnWelcomeCall
            // 
            this.btnWelcomeCall.Location = new System.Drawing.Point(51, 61);
            this.btnWelcomeCall.Name = "btnWelcomeCall";
            this.btnWelcomeCall.Size = new System.Drawing.Size(82, 23);
            this.btnWelcomeCall.TabIndex = 0;
            this.btnWelcomeCall.Text = "Welcome Call";
            this.btnWelcomeCall.UseVisualStyleBackColor = true;
            this.btnWelcomeCall.Click += new System.EventHandler(this.btnWelcomeCall_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(184, 161);
            this.Controls.Add(this.btnWelcomeCall);
            this.Name = "Form1";
            this.Text = "Welcome Call";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWelcomeCall;
    }
}

